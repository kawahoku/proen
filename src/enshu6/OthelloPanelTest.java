package enshu6;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class OthelloPanelTest
{
	public OthelloPanelTest()
	{
		JFrame frame = new JFrame("Othello");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(new OthelloPanel(), BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}
	
	public static void main(String[] args)
	{
		new OthelloPanelTest();
	}
}
