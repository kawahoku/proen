package enshu6;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class OthelloPanel extends JPanel
{
	public class Game
	{
		public static final boolean verbose = true;
		
		public static final int BLACK = +1;
		public static final int WHITE = -1;
		public static final int EMPTY =  0;
		public final int[][] direction =
				{{-1,-1},{-1, 0},{-1,+1},
				 { 0,-1},        { 0,+1},
				 {+1,-1},{+1, 0},{+1,+1}};

		private int nw;
		private int nh;
		
		private int[][] states;
		private int nmove;
		private int player;

		public Game(int nw, int nh)
		{
			this.nh = nh;
			this.nw = nw;
			states = new int[nh][nw];
			nmove = 0;

			player = BLACK;
			int i = nh / 2;
			int j = nw / 2;
			putStone(i, j, BLACK);
			putStone(i, j-1, WHITE);
			putStone(i-1, j, WHITE);
			putStone(i-1, j-1, BLACK);
		}
		
		public void action(int i, int j)
		{
			if( !onBoard(i, j) )
			{
				if( verbose )
				{
					String text = "===== ILLEGAL INPUT =====\n"
							    + "=  Click on the Board.  =\n"
							    + "=========================";
					System.out.println(text);
				}
			}
			else if( states[i][j] != EMPTY )
			{
				if( verbose )
				{
					String text = "===== ILLEGAL INPUT =====\n"
							    + "=  Click an Empty cell. =\n"
							    + "=========================";
					System.out.println(text);
				}
			}
			else
			{
				int[][] turnable = turnableStones(i, j, player);
				if( turnable.length != 0 )
				{
					System.out.println(ordinal(++nmove) + " th move:");
					putStone(i, j, player);
					for( int[] coord : turnable )
					{
						int bi = coord[0];
						int bj = coord[1];
						turnOver(bi, bj);
					}
					player *= -1;
				}
				else
				{
					if( verbose )
					{
						String text = "===== ILLEGAL INPUT =====\n"
								    + "= no stone to turn over.=\n"
								    + "=========================";
						System.out.println(text);
					}
				}
			}
		}
		
		public boolean onBoard(int i, int j)
		{
			return ( 0 <= i && i < nh  &&  0 <= j && j < nw );
		}
		
		public int[][] turnableStones(int i, int j, int player)
		{
			int[][] org = new int[nh*nw][2];
			int id = 0;
			for( int[] d : direction )
			{
				int k;
				for( k=1; ; k++ )
				{
					int bi = i + k * d[0];
					int bj = j + k * d[1];

					if( !onBoard(bi, bj) ) { k = 0; break; }
					
					if( states[bi][bj] == EMPTY ) { k = 0; break; }
					if( states[bi][bj] == player) { break; }
				}
				
				while( --k > 0 )
				{
					int bi = i + k * d[0];
					int bj = j + k * d[1];

					org[id++] = new int[] {bi, bj};
				}
			}
			return Arrays.copyOfRange(org, 0, id);
		}
		
		public void turnOver(int i, int j)
		{
			if( verbose )
			{
				System.out.println("["+ playerName(player) + "] ("+ i +","+ j +")  _TURN_OVER_" );
			}
			states[i][j] *= -1;
		}
		
		public void putStone(int i, int j, int color)
		{
			if( verbose )
			{
				System.out.println("["+ playerName(color) + "] ("+ i +","+ j +")  ____PUT____" );
			}
			states[i][j] = color;
		}
		
		public Color playerColor(int p)
		{
			Color color = Color.blue;
			if( p == BLACK ) color = Color.black;
			if( p == WHITE ) color = Color.white;
			return color;
		}
		
		public String playerName(int p)
		{
			String name = "NAME";
			if( p == BLACK ) name = "BLACK";
			if( p == WHITE ) name = "WHITE";
			return name;
		}
	}
	private int initWidth = 600;
	private int initHeight = 600;
	private int NW = 8;
	private int NH = 8;

	private int gridW = initWidth / NW;
	private int gridH = initHeight / NH;
	private int startX = 0;
	private int startY = 0;
	private int size = Math.min(initWidth, initHeight);
	private String title = "Othello";
	
	public static final Color boardColor = new Color(128, 255, 0);
	public static final Color bgColor = new Color(153, 153, 102);
	
	private Game game;

	public OthelloPanel()
	{
		game = new Game(NW, NH);
		setPreferredSize(new Dimension(initWidth, initHeight));
		addMouseListener(mouseAdapter());
	}
	
	public MouseAdapter mouseAdapter()
	{
		return new MouseAdapter() {
			public void mouseClicked(MouseEvent e)
			{
				int relX = e.getX() - startX;
				int relY = e.getY() - startY;

				int i = (0<=relY && relY<=size? relY/gridH : -1);
				int j = (0<=relX && relX<=size? relX/gridW : -1);
				System.out.println("(" + i + "," + j + ") clicked");
				game.action(i, j);
				System.out.println("");
				reTitle();
				repaint();
			}
		};
	}
	
	public void reTitle()
	{
		String t = title + " [" + game.playerName(game.player) + "'s Turn]";
		JFrame frame = (JFrame) getRootPane().getParent();
		frame.setTitle(t);
	}
	
	@Override
	public void paint(Graphics g)
	{
		int boardWidth = getWidth() - 6;
		int boardHeight = getHeight() - 6;
		size = Math.min(boardWidth, boardHeight);
		startX = 3 + (boardWidth - size) / 2;
		startY = 3 + (boardHeight - size) / 2;
		gridW = size / NW;
		gridH = size / NH;
		paintBoard(g, startX, startY, gridW, gridH);

		// paint stones
		for( int i=0; i<NH; i++ )
		{
			for( int j=0; j<NH; j++ )
			{
				int x = startX + j*gridW;
				int y = startY + i*gridH;

				int color = game.states[i][j];
				if( color != 0 )
				{
					paintStone(g, x, y, gridW, gridH, color);
				}
			}
		}
	}
	
	public void paintBoard(Graphics g, int x, int y, int w, int h)
	{
		// background
		g.setColor(bgColor);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		// player color
		g.setColor(game.playerColor(game.player));
		// g.drawRect(1, 1, getWidth()-3, getHeight()-3);
		g.drawRect(x-1, y-1, w*NW+2, h*NH+2);
		
		// board
		g.setColor(boardColor);
		g.fillRect(x, y, w*NW, h*NH);
		
		// grid
		g.setColor(Color.gray);
		for( int j=0; j<NW+1; j++ )
		{
			g.drawLine(x + j*w, y, x + j*w, y + h*NH);
		}
		for( int i=0; i<NH+1; i++ )
		{
			g.drawLine(x, y + i*h, x + w*NW, y + i*h);
		}
	}
	
	public void paintStone(Graphics g, int x, int y, int w, int h, int player)
	{
		g.setColor(game.playerColor(player));
		g.fillArc(x, y, w, h, 0, 360);
		g.setColor(Color.gray);
		g.drawArc(x, y, w, h, 0, 360);
	}
		
	public static String ordinal(int i)
	{
		String[] sufixes = {"st", "nd", "th", "th", "th", "th", "th", "th", "th", "th"};
		switch( i % 100 )
		{
		case 11:
		case 12:
		case 13: return i + "th";
		default: return i + sufixes[i%10];
		}
	}
}
