package enshu4;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

public class FileOutput
{
	public FileOutput(String fname)
	{
		try
		{
			PrintWriter pw = new PrintWriter(new File(fname));
			pw.println("This is ");
			pw.println("a sample program ");
			pw.println("of file output.");
			pw.close();
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		try
		{
			URL url = FileInput.class.getResource("../text.txt");
			System.out.println(url);
			File test = new File(url.toURI());
			new FileOutput(test.toString());
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}
}
