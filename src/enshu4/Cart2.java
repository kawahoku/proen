package enshu4;

import java.util.HashMap;
import java.util.Map;

public class Cart2
{
	private Map<String, Integer> item;
	
	public Cart2()
	{
		item = new HashMap<>();
		item.put("Chocolate", 1);
		item.put("Chips", 2);
		item.put("Candy", 4);
	}
	
	public void printAll()
	{
		for( Map.Entry<String, Integer> e : item.entrySet() )
		{
			System.out.println(e.getKey() + " x" + e.getValue());
		}
	}
	
	public static void main(String[] args)
	{
		Cart2 c2 = new Cart2();
		c2.printAll();
	}
}
