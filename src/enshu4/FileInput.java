package enshu4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

public class FileInput
{
	public FileInput(String filename)
	{
		FileReader fr;
		try
		{
			fr = new FileReader(new File(filename));
		}
		catch( IOException e )
		{
			e.printStackTrace();
			return;
		}

		BufferedReader br = new BufferedReader(fr);
		String line;
		try
		{
			while( (line = br.readLine()) != null )
			{
				System.out.println(line);
			}
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
	}

	// make sure that directory "lib" that contains "input.txt"
	// used as a Source Folder
	public static void main(String[] args)
	{
		try
		{
			URL url = FileInput.class.getResource("../input.txt");
			System.out.println(url);
			File input = new File(url.toURI());
			new FileInput(input.toString());
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}
}
