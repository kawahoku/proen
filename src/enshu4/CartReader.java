package enshu4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartReader
{
	private static final String separator = ",";
	private static final String escape = "//";
	
	private Map<String, Integer> item;


	public CartReader(String fname)
	{
		List<String> lines = fileToLines(fname);
		item = linesToMap(lines);
	}
	
	private List<String> fileToLines(String fname)
	{
		List<String> lines = new ArrayList<>();

		try
		{
			FileReader fr = new FileReader(new File(fname));
			BufferedReader br = new BufferedReader(fr);
			String line;
			while( (line = br.readLine()) != null )
			{
				lines .add(line);
			}
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}

		return lines;
	}
	
	private Map<String, Integer> linesToMap(List<String> lines)
	{
		Map<String, Integer> map = new HashMap<>();
		for( String line : lines )
		{
			String[] split = line.split(separator);
			if( line.startsWith(escape) || split.length != 2 )
			{
				continue;
			}
			String key = split[0].replace(" ", "");
			Integer val = Integer.valueOf(split[1].replace(" ", ""));

			if( map.containsKey(key) )
				map.put(key, map.get(key)+val);
			else
				map.put(key, val);
		}
		return map;
	}
	
	@Override
	public String toString()
	{
		String out = super.toString() + "\n";
		out += "========== item ==========\n";
		for( Map.Entry<String, Integer> e : item.entrySet() )
		{
			out += e.getKey() + " x" + e.getValue() + "\n";
		}
		out += "==========================\n";
		return out;
	}
	
	// Make sure that there's a Source Folder
	// that contains "reader.txt"
	public static void main(String[] args)
	{
		try
		{
			URL url = FileInput.class.getResource("../reader.txt");
			File input = new File(url.toURI());
			CartReader cr = new CartReader(input.toString());
			System.out.println(cr);
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}
}
