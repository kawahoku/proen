// B8TB2082
// Yoshito Kawasaki

package enshu1;

public class EMoneyCard
{
	private int balance;
	private String name;
	
	
	public EMoneyCard(String name)
	{
		this.name = name;
		balance = 0;
	}
	
	public void charge(int b)
	{
		balance += b;
	}
	
	public boolean pay(int b)
	{
		if( balance >= b )
		{
			balance -= b;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean transferFrom(EMoneyCard emc, int b)
	{
		if( emc.pay(b) )
		{
			charge(b);
			return true;
		}
		else
		{
			return false;
		}
	}

	public void print()
	{
		System.out.println("   name: " + name);
		System.out.println("balance: " + balance);
	}
	
	public static void main1()
	{
		EMoneyCard emc = new EMoneyCard("MyCard");

		emc.charge(100);
		emc.print();
		if( emc.pay(500) )
		{
			System.out.println("Success");
		}
		else
		{
			System.out.println("Fail");
		}
		
		emc.charge(1000);
		emc.print();
		if( emc.pay(500) )
		{
			System.out.println("Success");
		}
		else
		{
			System.out.println("Fail");
		}

		emc.print();
	}
	
	public static void main2()
	{
		EMoneyCard emc1 = new EMoneyCard("Card1");
		EMoneyCard emc2 = new EMoneyCard("Card2");
		emc1.charge(100);
		emc2.charge(100);
		emc1.print();
		emc2.print();
		if( emc1.transferFrom(emc2, 300) )
		{
			System.out.println("Success");
		}
		else
		{
			System.out.println("Fail");
		}

		emc2.charge(1000);
		emc1.print();
		emc2.print();
		if( emc1.transferFrom(emc2, 300) )
		{
			System.out.println("Success");
		}
		else
		{
			System.out.println("Fail");
		}

		emc1.print();
		emc2.print();
	}
	
	public static void main(String[] args)
	{
		main1();
		System.out.println("\n");
		main2();
	}
}
