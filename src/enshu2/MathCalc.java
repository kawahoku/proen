package enshu2;

public class MathCalc extends Calc
{
	public static final int RADIAN_MODE = 0;
	public static final int DEGREE_MODE = 1;

	private double base = Math.E;
	private int angle_mode = RADIAN_MODE;
	
	public void pow(double d)
	{
		val = Math.pow(val, d);
	}
	
	public void setLogBase(double d)
	{
		if( d <= 0 || d == 1 )
		{
			System.out.println("Error");
			return;
		}
		base = d;
	}
	
	public void log()
	{
		if( val <= 0 )
		{
			System.out.println("Error");
			return;
		}
		val = Math.log(val) / Math.log(base);
	}
	
	public void setRadianMode()
	{
		angle_mode = RADIAN_MODE;
	}
	
	public void setDegreeMode()
	{
		angle_mode = DEGREE_MODE;
	}
	
	public void sin()
	{
		switch( angle_mode )
		{
		case DEGREE_MODE: val = val * Math.PI / 180;
		case RADIAN_MODE: val = Math.sin(val);
		}
	}
	
	public void cos()
	{
		switch( angle_mode )
		{
		case DEGREE_MODE: val = val * Math.PI / 180;
		case RADIAN_MODE: val = Math.cos(val);
		}
	}
	
	@Override
	public void div(double d)
	{
		if( d == 0 )
		{
			System.out.println("Error");
			return;
		}
		val /= d;
	}
	

	public static void main(String[] args)
	{
		MathCalc mc = new MathCalc();
		
		// sin(30 [deg])
		mc.setDegreeMode();
		mc.set(30);
		mc.sin();
		mc.print();
		
		// sin(pi / 2)
		mc.setRadianMode();
		mc.set(Math.PI);
		mc.div(2);
		mc.sin();
		mc.print();
		
		// log2(1024)
		mc.setLogBase(2);
		mc.set(1024);
		mc.log();
		mc.print();
		
		// log2(1024)^5
		mc.pow(5);
		mc.print();
		
		// log10(log2(1024)^5)
		mc.setLogBase(10);
		mc.log();
		mc.print();
		
		// Error check
		mc.div(0);
		mc.set(0);
		mc.log();
		mc.setLogBase(-1);
		mc.setLogBase(1);
	}
}
