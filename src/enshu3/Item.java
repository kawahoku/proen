package enshu3;

public class Item
{
	protected String name;
	protected int price;
	
	public Item(String name, int price)
	{
		this.name = name;
		this.price = price;
	}
	
	@Override
	public boolean equals(Object o)
	{
		Item item = (Item) o;
		return name.equals(item.name) & (price == item.price);
	}
	
	@Override
	public String toString()
	{
		return name + "(" + price + "JPY)";
	}
	
	public void print()
	{
		System.out.println(toString());
	}
}
