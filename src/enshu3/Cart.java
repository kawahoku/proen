package enshu3;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class Cart
{
	private Vector<Item> items;
	private int budget;
	private int sum;
	public static boolean verbose = true;
	
	public Cart(int budget)
	{
		this.budget = budget;
		sum = 0;
		items = new Vector<>();
	}
	
	public void add(String name, int price)
	{
		Item item = new Item(name, price);

		String log = "[ADD]    " + item;
		if( price + sum <= budget )
		{
			items.add(item);
			sum += price;
			log += " [SUCCEEDED]";
		}
		else
		{
			log += " [FAILED]";
		}

		if( verbose )
			System.out.println(log);
	}
	
	public void remove(String name, int price)
	{
		Item target = new Item(name, price);
		
		String log = "[REMOVE] " + target;
		boolean succeeded = false;
		for( Item item : items )
		{
			if( item.equals(target) )
			{
				items.remove(item);
				sum -= price;
				succeeded = true;
				break;
			}
		}
		log += (succeeded? " [SUCCEEDED]":" [FAILED]");

		if( verbose )
			System.out.println(log);
	}
	
	public void printPlan()
	{
		Map<String, Integer> count = new HashMap<>();
		for( Item item : items )
		{
			String str = item.toString();
			if( count.containsKey(str) )
				count.put(str, count.get(str) + 1);
			else
				count.put(str, 1);
		}
		
		String out = "";
		for( Map.Entry<String, Integer> e : count.entrySet() )
		{
			out += e.getKey() + " x" + e.getValue() + "\n";
		}
		out += "sum: " + sum + "JPY\n";

		System.out.println(out);
	}
	
	public static void main(String[] args)
	{
		Cart sc = new Cart(300);
		
		// add snacks
		sc.add("Chocolate", 120);
		sc.add("Chips", 120);
		sc.add("PopCorn", 110);
		
		// change Chips into Candy
		sc.remove("Chips", 120);
		sc.add("Candy", 10);
		
		// add PopCorn again
		sc.add("PopCorn", 110);
		
		// print shopping plan
		System.out.println("---- Items ----");
		sc.printPlan();
		
		// remove Chocolate
		sc.remove("Chocolate", 120);
		
		// add 3 Candy
		sc.add("Candy", 10);
		sc.add("Candy", 10);
		sc.add("Candy", 10);
		
		// add PopCorn
		sc.add("PopCorn", 110);
		
		// print shopptin plan
		System.out.println("---- Items ----");
		sc.printPlan();
	}
}
