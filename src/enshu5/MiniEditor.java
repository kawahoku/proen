package enshu5;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MiniEditor extends JFrame
{
	public class BorderPanel extends JPanel
	{
		public BorderPanel() { super(new BorderLayout()); }
		public void setCenter(Component c){ add(c, BorderLayout.CENTER); }
		public void setTop(Component c)   { add(c, BorderLayout.PAGE_START); }
		public void setBottom(Component c){ add(c, BorderLayout.PAGE_END); }
		public void setLeft(Component c)  { add(c, BorderLayout.LINE_START); }
		public void setRight(Component c) { add(c, BorderLayout.LINE_END); }
	}
	public class ScrollPanel extends BorderPanel
	{
		protected JScrollPane scroll;
		public ScrollPanel(Component c)
		{
			scroll = new JScrollPane(c);
			setCenter(scroll);
		}
	}

	private BorderPanel panel;

	private BorderPanel header;
	private JLabel label;
	private JTextField field;
	private JButton loadButton;
	
	private BorderPanel subHeader;
	private JButton saveButton;

	private ScrollPanel scroll;
	private BorderPanel canvas;
	private JTextArea area;
	
	public MiniEditor()
	{
		SwingUtilities.invokeLater(()-> {initialize();});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(320, 240));
	}
	
	public void initialize()
	{
		label = new JLabel("File:");
		field = new JTextField("Enter your-file-name.");
		loadButton = new JButton("Load");
		loadButton.addActionListener(e -> load());
		header = new BorderPanel();
		header.setLeft(label);
		header.setCenter(field);
		header.setRight(loadButton);
		
		saveButton = new JButton("save");
		saveButton.addActionListener(e -> save());
		subHeader = new BorderPanel();
		subHeader.setCenter(header);
		subHeader.setRight(saveButton);
		

		area = new JTextArea("text here");
		scroll = new ScrollPanel(area);

		panel = new BorderPanel();
		panel.setTop(subHeader);
		panel.setCenter(scroll);
		
		getContentPane().add(panel);
	}
	
	public void save()
	{
		try
		{
			File file = nameToFile(field.getText());
			String text = area.getText();
			System.out.println("write to: " + file);
			System.out.println(text);
			writeToFile(file, text);
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}
	
	public void load()
	{
		try
		{
			File file = nameToFile(field.getText());
			System.out.println("open: " + file);
			String text = fileToText(file);
			System.out.println(text);
			area.setText(text);
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}
	
	// expect input is full-path
	// or relative-path to source folder
	public File nameToFile(String input) throws URISyntaxException
	{
		File file = new File(input);
		if( !file.exists() )
		{
			URL url = MiniEditor.class.getResource("../" + input);
			file = new File(url.toURI());
		}
		return file;
	}
	
	public String fileToText(File file) throws IOException
	{
		String text = "";
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		while( (line = br.readLine()) != null )
		{
			text += line + "\n";
		}
		return text;
	}

	public void writeToFile(File file, String text) throws IOException
	{
		FileWriter fw = new FileWriter(file);
		fw.write(text);
		fw.close();
	}
	
	
	public static void main(String[] args)
	{
		MiniEditor me = new MiniEditor();
		me.setVisible(true);
	}
}
